/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package graphutility;
import java.util.ArrayList;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.util.Duration;

/**
 *
 * @author Vasia
 */
public class Node extends Pane {
    
    private Circle circle = new Circle();
    private Label num;
    
    private ArrayList<Connection> connections = new ArrayList<>();
    private ArrayList<Node> connectedNodes = new ArrayList<>();
    //private Set<Node> connectedNodes = new TreeSet<Node>();
    private boolean selected = false;
    
    
    private int color;
    
    private Color defaultFill = Color.GREEN;
    private Color defaultStroke = Color.PALEGREEN;
    
    private Color selectedFill = Color.BLUE;
    private Color selectedStroke = Color.WHITE;
    
    private int degree = 0;
    
    public Node(double x, double y, double radius, int count){
        circle.setCenterX(x);
        circle.setCenterY(y);
        circle.setRadius(radius);
        circle.setFill(defaultFill);
        circle.setStroke(defaultStroke);
        circle.setStrokeWidth(2);
        
        num = new Label(count+"");

        if (count >= 10)
            num.relocate(x-8,y-9);
        else num.relocate(x-4,y-9);
        
        num.setFont(Font.font("Consolas", 16));
        num.setTextFill(Color.WHITE);
        num.setVisible(true);
        
        getChildren().addAll(circle,num);
        
       
        
        
        
        
    }
    //Overriden methods
    public double getCenterX(){
        return circle.getCenterX();
    }
    public double getCenterY(){
        return circle.getCenterY();
    }
    
    public ArrayList<Connection> getConnections(){
        return connections;
    }
    
    public ArrayList<Node> getNodes(){
        return connectedNodes;
    }
    
    public void addNode(Node node){
        connectedNodes.add(node);
    }
    
    public void deselect(){
        circle.setFill(defaultFill);
        selected = false;
    }
    public boolean removeNode(Node node){
        return connectedNodes.remove(node);
    }
    
    public void incDegree(){
        degree +=1;
    }
    
    public int getDegree(){
        return degree;
    }
    
    public void decDegree(){
        degree -= 1;
    }
    
    public void setDegree(int deg){
        degree = deg;
    }
    
    public void addConnection(Connection con){
        connections.add(con);
    }
    
    public Label getLabel(){
        return num;
    }
    
    public void setLabelText(String text){
        num.setText(text);
    }
    
    public void setLabel(Label label){
        num = label;
    }
    
    public void moveNode(double x, double y){
        circle.setCenterX(x);
        circle.setCenterY(y);
        if (Integer.parseInt(num.getText()) >= 10)
            num.relocate(x-8,y-9);
        else num.relocate(x-4,y-9);
    }
    
    public int getColor(){
        return color;
    }
    
    public void setColor(int color){
        this.color = color;
    }
    
    public boolean removeConnection(Connection con){
        connectedNodes.remove(con.getFirst());
        connectedNodes.remove(con.getSecond());
        decDegree();
        return connections.remove(con);
    }
    
    public void toggleSelected(){
       if (!selected){
           circle.setFill(selectedFill);
           selected = true;
       }
           
       else {
           circle.setFill(defaultFill);
           selected = false;
       }
    }
    
    @Override
    public String toString(){
        return (num.getText()+"");
    }
    
    public boolean isSelected(){
        return selected;
    }
    
}
