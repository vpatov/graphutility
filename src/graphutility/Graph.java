/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package graphutility;

import javafx.scene.shape.Line;
import javafx.scene.paint.Color;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;
import javafx.scene.layout.Pane;
import java.util.*;


/**
 *
 * @author Vasia
 */
public class Graph extends Pane {
    private ArrayList<Node> nodes = new ArrayList<>();
    private ArrayList<Connection> cons = new ArrayList<>();
    private boolean connected;
    
    public Graph(Collection<Node> nodes,Collection<Connection> cons){
        for (Node node: nodes){
            this.nodes.add(node);
        }
        for (Connection con: cons){
            this.cons.add(con);
        }
    }
    
    public Graph(){
        
    }
    
    public ArrayList<Node> getNodes(){
        return nodes;
    }
    public ArrayList<Connection> getConnections(){
        return cons;
    }
    
    
    
    
    public int size(){
        return nodes.size();
    }
    
    public int numEdges(){
        return cons.size();
    }
    
    
    
    
    
    public void addNode(Node node){
        nodes.add(node);
    }
    public void addCon(Connection con){
        cons.add(con);
    }
    
    public boolean isConnected(){
        return connected;
    }
    
    public void determineConnectedness(){
        connected = (breadthFirst(nodes.get(0)).size() == nodes.size());
    }
    
     public ArrayList<Node> breadthFirstRecur(Node root,ArrayList<Node> traversed){
       if (!traversed.contains(root))
           traversed.add(root);
       for (int i = 0; i < root.getNodes().size();i++){
           Node node = root.getNodes().get(i);
           if (!traversed.contains(node)){
               breadthFirstRecur(node,traversed);
           }
       }
       return traversed;
    }
    
    public ArrayList<Node> breadthFirst(Node root){
        ArrayList<Node> traversed = new ArrayList<>();
        return breadthFirstRecur(root,traversed);
    }
    
    
    //shifts the graph x and y units
    public void shiftGraph(double xOff, double yOff){
        for (Node node: nodes){
            node.relocate(node.getCenterX() + xOff, node.getCenterY() + yOff);
        }
        
        for (Connection con: cons){
            con.setStartX(con.getStartX() + xOff);
            con.setStartY(con.getStartY() + yOff);
            
            con.setEndX(con.getEndX() + xOff);
            con.setEndY(con.getEndY() + yOff);   
        }
    }
    
   
    
    
}
