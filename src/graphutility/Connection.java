/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package graphutility;

import javafx.scene.shape.Line;
import javafx.scene.paint.Color;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;


/**
 *
 * @author Vasia
 */
public class Connection extends Line{
    
    private final Node first,second;
    private final Timeline animation;
    
    public Connection(Node a, Node b){
        first = a;
        second = b;
        setStartX(a.getCenterX());
        setStartY(a.getCenterY());
        setEndX(a.getCenterX());
        setEndY(a.getCenterY());
        
        a.addNode(b);
        b.addNode(a);
        a.addConnection(this);
        b.addConnection(this);
        
        a.incDegree();
        b.incDegree();
        
        animation = new Timeline(new KeyFrame(Duration.millis(20), e -> growLine(100)));
        animation.setCycleCount(100);
        animation.play(); // Start animation

        


        setStroke(Color.BLUE);
        setStrokeWidth(2);
        
    }
    
    public double[] slope(double x1, double y1, double x2, double y2){
        double[] ans = {(y2-y1),(x2-x1)};
        return ans;
    }
    
    
    
    public Node getFirst(){
        return first;
    }
    
    public Node getSecond(){
        return second;
    }
    
   /* protected void moveBall() {
// Check boundaries
if (x < radius || x > getWidth() - radius) {
dx *= -1; // Change ball move direction
}
if (y < radius || y > getHeight() - radius) {
dy *= -1; // Change ball move direction
}
// Adjust ball position
x += dx;
y += dy;
circle.setCenterX(x);
circle.setCenterY(y);
}
    */
    private void growLine(int step) {
        double[] coeffs = slope(first.getCenterX(), first.getCenterY(), second.getCenterX(), second.getCenterY());
        
        
        setEndX(getEndX() + (coeffs[1]/step));
        setEndY(getEndY() + (coeffs[0]/step));
    }
}
