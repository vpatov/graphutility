//TO-IMPLEMENT LIST
/*  
    //Things to ask professor:
    How to best implement drag/drop to move nodes

    How to combine the Node(Circle) and Label into one Object, so that they can be added together.
    Turn it into a NodePane object for each one?


    0. Remove All/ Remove Connections button

    0. Put labels on top of nodes, and not in their own pane.


    
    (No specific priority set here)
    DONE========================================
    1. N-Partite Graph Construction
        a. Cluster Arranger
        b. Inner Cluster Arranger
    DONE========================================
        
    2. Create K-Graph Clear Dialog
        a. Allow checkoption for (Do not show this again)
        b. say it is recommended to clear before constructing

    3. Drag and drop individual nodes, Drag and drop connected clusters.

    4. Help, About, JPG Buttons (Top Right)

    5. Menu Bar (Operations will include FileOpen, FileSave, Export .JPG/.GIF,
                 Preferences (which will include Node Size, Node Color, Connection Thickness,
                              Screen Size, Labels:On/Off)
                 Close File, Undo, Redo
    
    6. Select Multiple Nodes with a rectanglular selection marquee
        a. allow for them to be all connected, all removed, duplicated, copied 

    7. Hamiltonian Circuit Algorithm (polynomial time algorithm found online)
    8. Euler Path, Circuit, Trail ,etc
    9. Graph Construction Modes: Directed/Undirected, Simple/Multigraph 
        a. this would include allowing self-loops.
        
    10. Rotate entire graph or sections of graph

    11. Add List of Pre-Constructed Artsy Graphs
        e.g. smiley face, constellations, solar system

    12. Isomorphism of two graph selections

    13. Coloring of Nodes
        a. In main scene include checkbox (Multicolored),
           when checked, would ungrey the Color button (which would usually
           be grey), and would allow user to have up to whatever amount of colors he wants.
           
           If unchecked, and nodes are multicolored, revert all nodes back to the default color, but save their 
           multicolored colors

        b. function that finds a minimal coloring of the graph and colors it.
        
    
    14. View Grid Option (Allow Grid to have various dimensions, rarity, etc.)
        a. include snap to grid option

    15. Test wether graph is planar (if this, show planar representation)

    16. Highlight minimum spanning tree
    
*/

package graphutility;

import java.util.ArrayList;
import javafx.animation.Timeline;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.collections.ObservableList;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Control;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Vasia
 */
public class GraphUtility extends Application {
    
    //Scene Parameters
    private final int sW = 730;
    private final int sH = 440;
    
    private final double canvasCenterX = (248.5 + 18);
    private final double canvasCenterY = (178.5 + 73);
    
    private int nodeCount = 0;
    private int kn = 2;
    
    //Scenes and Panes - their order shows their layering. root on bottom, buttons on top.
    Scene mainScene;
    StackPane root;
    Pane connectionPane;
    Pane nodePane;
    Pane componentPane;
    GridPane buttonPane;
    //Pane statusPane;
    
    //Background Image
    Image background = new Image("Background.gif");
    Image backPane = new Image("BackPane.jpg");
    ImageView bg = new ImageView(background);
    ImageView bp = new ImageView(backPane);
    
    
    //Buttons and ComboBoxes
    Control[] controls = new Control[12];
    Button add = new Button("Add");
    Button remove = new Button("Remove");
    Button connect = new Button("Connect");
    Button kGraph = new Button("K-N Graph");
    Button clear = new Button("Clear");
    Button move = new Button("Move");
    Button connectAll = new Button("Connect All");
    Button nPartite = new Button("N-Partite Graph");
    ComboBox kChoice = new ComboBox();
    TextField nPartiteGroups = new TextField();
    
    
    
    
    
    //Labels
    Label statusLabel;
    
    
    //Nodes are organized into graphs. default amount of graphs is first 0, then upon
    //first node creation graph size is 1.
    public ArrayList<Graph> graphs = new ArrayList<>();
    public Graph currentGraph;
    
    
    //Node, Node Degrees and Connections ArrayList
    public ArrayList<Node> nodes = new ArrayList<>();
    public ArrayList<Connection> cons = new ArrayList<>();
    public ArrayList<Integer> degrees = new ArrayList<>();
    
    public Node[] pendCons = new Node[2]; //pendCons = pending Connections
    public int[] groups;
    
    //Animations
    private Timeline animation;
    
    
    //Booleans and Constants
    
        
    @Override
    public void start(Stage primaryStage) {
        //pane setup
        root = new StackPane();
        connectionPane = new Pane();
        nodePane = new Pane();
        componentPane = new Pane();
        buttonPane = new GridPane();
        //statusPane = new Pane();
        statusLabel = new Label("");
        
        
        
        //mainScene Setup and size assignment
        mainScene = new Scene(root,sW,sH);
        primaryStage.setResizable(false);
        

        //Adding of all the panes
        root.getChildren().add(bp);
        root.getChildren().addAll(connectionPane,nodePane);
        root.getChildren().add(bg);
        root.getChildren().add(componentPane);
        //root.getChildren().add(statusPane);
        componentPane.getChildren().add(buttonPane);
        
        
        //Buttons array intialziation
        controls[0] = add;
        controls[1] = remove;
        controls[2] = connect;
        controls[3] = kGraph;
        controls[4] = clear;
        controls[5] = move;
        controls[6] = connectAll;
        controls[7] = nPartite;
        controls[8] = kChoice;
        controls[9] = nPartiteGroups;
        
       
        
        //Adding of buttons and the status label to the ButtonPane (topmost pane)
        buttonPane.add(add,0,0);
        buttonPane.add(remove,1,0);
           
        buttonPane.add(move,0,1);
        buttonPane.add(clear,1,1);
        
        buttonPane.add(connect,0,2);
        buttonPane.add(connectAll,1,2);
        
        buttonPane.add(kGraph,0,3);
        buttonPane.add(kChoice,1,3);
        
        buttonPane.add(nPartite,0,4);
        buttonPane.add(nPartiteGroups,1,4);
       
        //statusPane.getChildren().add(statusLabel);
        buttonPane.relocate(525,75);
        
        
        
        //Setting attributes and dimensions of the three buttons and the status label.
        kChoice.setValue(2);
        kChoice.setVisibleRowCount(6);
       
        statusLabel.setFont(Font.font("Consolas", 18));
        statusLabel.setStyle("-fx-text-fill: #7b68ee; -fx-font-weight: bold");
        statusLabel.setLayoutX(525);    
        statusLabel.setLayoutY(410);
        
        nPartiteGroups.setMaxSize(105, 20);
        
        
        //Setting general control parameters
         for (int i = 0; i< 9; i++){
             controls[i].setMaxWidth(105);
             controls[i].setMinWidth(105);
             
         }

        
        //ADDING NUMBERS 2-26 to the Combo Box kChoice (for K-N graph construction)
        for (int i = 2; i < 26; i++){
            kChoice.getItems().add(i);
        }
        
        //KCHOICE n SELECTION
        kChoice.setOnAction((event) -> {
            kn = (Integer)kChoice.getSelectionModel().getSelectedItem();     
        });
        
        
        
        //ADDING NODES
        mainScene.addEventFilter(MouseEvent.MOUSE_PRESSED, (MouseEvent mouseEvent) -> {
            System.out.println(mouseEvent.getSceneX());
            System.out.println(mouseEvent.getSceneY());
            if (statusLabel.getText().equals("Add")){
                double x = mouseEvent.getSceneX();
                double y = mouseEvent.getSceneY();
                
                if (x < 515  && y < 430 && x > 18 && y > 75){
                    Node node = new Node(x,y,15,++nodeCount);
                    nodes.add(node);
                    nodePane.getChildren().add(node);
                    
                }
                
            }
        });
        

        
        
        
        //REMOVING NODES/CONNECTIONS
        mainScene.addEventFilter(MouseEvent.MOUSE_PRESSED, (MouseEvent mouseEvent) -> {
            if (statusLabel.getText().equals("Remove")){
                
                double x = mouseEvent.getSceneX();
                double y = mouseEvent.getSceneY();
                
                for (Node node: nodes){
                    if (node.contains(x,y)){
                        nodes.remove(node);
                        nodePane.getChildren().remove(node);
                        
                        
                        ArrayList<Connection> connections = node.getConnections();
                        for (Connection con: connections){
                            connectionPane.getChildren().remove(con);
                        }
                        
                        break;     
                    }
                }
                for (Connection con: cons){
                    if (con.contains(x,y)){
                        cons.remove(con);
                        connectionPane.getChildren().remove(con);
                        con.getFirst().removeConnection(con);
                        con.getFirst().removeNode(con.getSecond());
                        con.getSecond().removeConnection(con);
                        con.getSecond().removeNode(con.getFirst());
                        break;
                        
                    }
                }
                
            }
        });
        
        //CONNECTING NODES
        mainScene.addEventFilter(MouseEvent.MOUSE_PRESSED, (MouseEvent mouseEvent) -> {
            if (statusLabel.getText().equals("Connect")){
                
                //get mouse coordiantes
                double x = mouseEvent.getSceneX();
                double y = mouseEvent.getSceneY();
                
                //if no nodes selected yet
                if (pendCons[0] == null){
                    for (Node node: nodes){
                        if (node.contains(x,y)){
                            node.toggleSelected();
                            pendCons[0] = node;
                            break;     
                        }
                    }
                }
                
                else if (pendCons[1] == null){
                    for (Node node: nodes){
                        if (node.contains(x,y) && !node.equals(pendCons[0])
                                && !(node.getNodes().contains(pendCons[0]))){
                            node.toggleSelected();
                            pendCons[1] = node;
                            
                    
                            
                            
                            
                            Connection con = connect(pendCons[0],pendCons[1]);
                            
                            
                            connectionPane.getChildren().add(con);
                            
                            pendCons[0].toggleSelected();     
                            pendCons[1].toggleSelected();
                            
                            pendCons[0] = null;
                            pendCons[1] = null;
                            break;
                        }
                        if (node.contains(x,y) && node.equals(pendCons[0])){
                            node.deselect();
                            pendCons[0] = null;
                        }
                    }
                }
            }
        });
        
        
        //ADD BUTTON
        add.setOnAction((ActionEvent event) -> {
            
            if (statusLabel.getText().equals("Add"))
                resetModes();
            else {resetModes(); statusLabel.setText("Add");}         
        });
        
        //REMOVE BUTTON
        remove.setOnAction((ActionEvent event) -> {
            if (statusLabel.getText().equals("Remove"))
                resetModes();
            else {resetModes(); statusLabel.setText("Remove");}
        });
        
        //CONNECT BUTTON
        connect.setOnAction((ActionEvent event) -> {
            if (statusLabel.getText().equals("Connect"))
                resetModes();
            else {resetModes(); statusLabel.setText("Connect");}
        });
        
        //MOVE BUTTON
        move.setOnAction((ActionEvent event) -> {
            int[] groups = {3,3,2,3,2,3,2,3};
            nPartiteStraightGraph(2,groups);
            //rotateNode(nodes.get(0),4,nodes.get(1));
            
                    
                        
                    
            if (statusLabel.getText().equals("Move"))
                resetModes();
            else {resetModes(); statusLabel.setText("Move");}
        });
        
         //CONNECTALL BUTTON
        connectAll.setOnAction((ActionEvent event) -> {
            connectAll();
        });
       
         //CONNECTALL BUTTON
        nPartiteGroups.setOnAction((ActionEvent event) -> {
            
            
        });
        
        
        
        //Clear BUTTON
        
        /////////THIS CODE CAN BE SHORTENED, the if else statements contain redundant code, general initialization can
        ///////// be taken outside and made uncondintional, and then text/button setup would be conditional.
        ///////// not highest priority
        clear.setOnAction((ActionEvent event) -> {
            resetModes();
            
            if (nodes.isEmpty()){
                final Stage dialogStage = new Stage();
                dialogStage.initModality(Modality.WINDOW_MODAL);

                Label exitLabel = new Label("There is nothing to clear, the canvas is empty.");
                exitLabel.setFont(Font.font("Segoe UI",FontWeight.SEMI_BOLD,16));
                exitLabel.setAlignment(Pos.BASELINE_CENTER);

                Button okBtn = new Button("OK");
                okBtn.setOnAction((ActionEvent eventY) -> {                
                    dialogStage.close();
                });

                HBox hBox = new HBox();
                hBox.setAlignment(Pos.CENTER);
                hBox.setSpacing(20.0);
                hBox.getChildren().addAll(okBtn);

                VBox vBox = new VBox();
                vBox.setSpacing(20.0);
                vBox.getChildren().addAll(exitLabel, hBox);

                dialogStage.setScene(new Scene(vBox));
                dialogStage.show();
            }
            else {
                final Stage dialogStage = new Stage();
                dialogStage.initModality(Modality.WINDOW_MODAL);

                Label exitLabel = new Label("Are you sure you want to clear the canvas?");
                exitLabel.setAlignment(Pos.BASELINE_CENTER);

                Button yesBtn = new Button("Yes");
                yesBtn.setOnAction((ActionEvent eventY) -> {                
                    dialogStage.close();
                    clear();

                });

                Button noBtn = new Button("No");
                noBtn.setOnAction((ActionEvent eventN) -> {
                        dialogStage.close();   
                });

                HBox hBox = new HBox();
                hBox.setAlignment(Pos.CENTER);
                hBox.setSpacing(20.0);
                hBox.getChildren().addAll(yesBtn, noBtn);

                VBox vBox = new VBox();
                vBox.setSpacing(20.0);
                vBox.getChildren().addAll(exitLabel, hBox);

                dialogStage.setScene(new Scene(vBox));
                dialogStage.show();
            }
        });
        
        
        //KGRAPH BUTTON
        kGraph.setOnAction((ActionEvent event) -> {
            resetModes();
            if (nodes.isEmpty()){
                createKGraph(kn);
            }
            
            else {
                final Stage dialogStage = new Stage();
                dialogStage.initModality(Modality.WINDOW_MODAL);

                Label exitLabel = new Label("Creating a K-Graph will clear the canvas. Continue?");
                exitLabel.setAlignment(Pos.BASELINE_CENTER);

                Button yesBtn = new Button("Yes");
                yesBtn.setOnAction((ActionEvent eventY) -> {                
                    dialogStage.close();
                    clear();
                    createKGraph(kn);

                });

                Button noBtn = new Button("No");
                noBtn.setOnAction((ActionEvent eventN) -> {
                        dialogStage.close();   
                });

                HBox hBox = new HBox();
                hBox.setAlignment(Pos.BASELINE_CENTER);
                hBox.setSpacing(40.0);
                hBox.getChildren().addAll(yesBtn, noBtn);

                VBox vBox = new VBox();
                vBox.setSpacing(40.0);
                vBox.getChildren().addAll(exitLabel, hBox);

                dialogStage.setScene(new Scene(vBox));
                dialogStage.show();
            }
    });
        
        
    //NPARTITE BUTTON
        nPartite.setOnAction((ActionEvent event) -> {
            resetModes();
            if (nodes.isEmpty()){
                String values = nPartiteGroups.getText();
                if (parseGroups(values,kn)){
                    nPartiteStraightGraph(kn,groups);
                }
            }
            
            else {
                final Stage dialogStage = new Stage();
                dialogStage.initModality(Modality.WINDOW_MODAL);

                Label exitLabel = new Label("Creating a K-Graph will clear the canvas. Continue?");
                exitLabel.setAlignment(Pos.BASELINE_CENTER);

                Button yesBtn = new Button("Yes");
                yesBtn.setOnAction((ActionEvent eventY) -> {                
                    dialogStage.close();
                    clear();
                    
                    String values = nPartiteGroups.getText();
                    if (parseGroups(values,kn)){
                        nPartiteStraightGraph(kn,groups);
                    }

                });

                Button noBtn = new Button("No");
                noBtn.setOnAction((ActionEvent eventN) -> {
                        dialogStage.close();   
                });

                HBox hBox = new HBox();
                hBox.setAlignment(Pos.BASELINE_CENTER);
                hBox.setSpacing(40.0);
                hBox.getChildren().addAll(yesBtn, noBtn);

                VBox vBox = new VBox();
                vBox.setSpacing(40.0);
                vBox.getChildren().addAll(exitLabel, hBox);

                dialogStage.setScene(new Scene(vBox));
                dialogStage.show();
            }
    });
        
        
        
        
        
        
        
        //Set App Title and show Stage
        
        primaryStage.setTitle("Graph Utility");
        primaryStage.setScene(mainScene);
        primaryStage.show();
    }
    
    
    
    
//////////////////////////////////////////////////////////////////
    /// METHODS 
    public Connection connect(Node a, Node b){
        Connection con = new Connection(a,b);
        cons.add(con);
        return con;
    }
    
    public void resetModes(){
        statusLabel.setText("");
        pendCons[0] = null;
        pendCons[1] = null;
        for (Node node: nodes)
            node.deselect();
    }
    
    public boolean parseGroups(String values, int n){
        String[] nums = values.split(",");
        
         if (nums.length != n)
            return false;
         
        int[] ans = new int[n];
        int i = 0;
        for (String num: nums){
            char[] digs = num.toCharArray();
            for (char c: digs){
                if ((int)c < 48 || (int)c > 57)
                    return false;
            }
            ans[i] = Integer.parseInt(num);
            i++;
        }
        groups = ans;
        return true;
    }
    
    
    public void clear(){
        
        for (Node node: nodes){
            nodePane.getChildren().remove(node);
        }
        connectionPane.getChildren().removeAll(cons);
        nodes = new ArrayList<>();
        cons = new ArrayList<>();
        pendCons = new Node[2];
        nodeCount = 0;
    }
    
    
    
 
    
    
    public void createKGraph(int size){

            double[] steps = degreeSeperation(size);
            for (double deg: steps){
                double x = (157*Math.sin((deg) * (Math.PI/180))) + (canvasCenterX);
                double y = (157*Math.cos((deg) * (Math.PI/180))) + (canvasCenterY);
                Node node = new Node(x,y,15,++nodeCount);
                nodes.add(node);
                nodePane.getChildren().add(node);
            }
            
            
            //Connect all the nodes
            //Basically what connectAll does
            for (int i = 0; i < nodes.size(); i++){
                for (int j = i+1; j < nodes.size(); j++){
                    Connection con = connect(nodes.get(i),nodes.get(j));
                    cons.add(con);
                    connectionPane.getChildren().add(con);
                }
            }
            
            
        }
    
    public void connectAll(){
        for (int i = 0; i < nodes.size(); i++){
                for (int j = i+1; j < nodes.size(); j++){
                    if (!nodes.get(i).getNodes().contains(nodes.get(j))){
                        Connection con = connect(nodes.get(i),nodes.get(j));
                    cons.add(con);
                    connectionPane.getChildren().add(con);
                    }
                    
                }
            }
    }
    
    public void connectAllDifferentColors(){
        for (int i = 0; i < nodes.size(); i++){
                for (int j = i+1; j < nodes.size(); j++){
                    if (!nodes.get(i).getNodes().contains(nodes.get(j))){
                        if (nodes.get(i).getColor() != nodes.get(j).getColor()){
                            Connection con = connect(nodes.get(i),nodes.get(j));
                            cons.add(con);
                            connectionPane.getChildren().add(con);
                        }
                    }  
                }
            }
    }
    
    public double[] degreeSeperation(int steps){
        double[] ans = new double[steps];
        double spread = 360.0/steps;
        for (int i = 0; i < steps; i++){
            ans[i] = (spread*i) % 360;
                    
        }
        return ans;
    }
    
    public double[] linearSeperation(int steps){
        int start = (steps - 1) * (-20);
        double[] ans = new double[steps];
        for (int i = 0; i < steps; i++)
            ans[i] = start + (40*i);
        return ans;   
    }
    
    public double slopeToAngle(double slope){
        System.out.println("slope is: " + slope);
        return Math.atan(slope);
        
    }
    
    //The same way you created the kGraph, with the nodes evenly spread out along a circle,
    //That same hierarchy another level, with n-partiteness. so, n will be "steps", and 
    //will similarly colored nodes will be clustered, and there will be a total of n clusters.
    //
    
    //input will come from a combox box that lets you choose either 2,3,4,5, or 6-partite graph
    //amount in each cluster in text field as comma seperated value (e.g. 3,3,3)
    
    //Preconditions, groups.length == n
    public void nPartiteClusterGraph(int n,int[] groups){
        double[] steps = degreeSeperation(n);
        double externalRad = 125;
        double internalRad = 20;
        
        for (int i = 0; i < n; i++){
                double clusterX = (externalRad*Math.sin( (steps[i])*(Math.PI/180) )) + (canvasCenterX);
                double clusterY = (externalRad*Math.cos( (steps[i])*(Math.PI/180) )) + (canvasCenterY);
                
                double[] clusterSteps = degreeSeperation(groups[i]);
                
                for (double clust: clusterSteps){
                    double x = ((internalRad+(3*n))*Math.sin( (clust)*(Math.PI/180) )) + (clusterX);
                    double y = ((internalRad+(3*n))*Math.cos( (clust)*(Math.PI/180) )) + (clusterY);
                    
                    Node node = new Node(x,y,15,++nodeCount);
                    nodes.add(node);
                    node.setColor(i);
                    nodePane.getChildren().add(node);
                }
                
                
            }
        
        //Connect all differently colored nodes
        for (int i = 0; i < nodes.size(); i++){
                for (int j = i+1; j < nodes.size(); j++){
                    if (nodes.get(i).getColor() != nodes.get(j).getColor()){
                    Connection con = connect(nodes.get(i),nodes.get(j));
                    cons.add(con);
                    connectionPane.getChildren().add(con);
                    }
                }
            }
    }
    
    public void nPartiteStraightGraph(int n,int[] groups){
        double[] steps = degreeSeperation(n);
        double externalRad = 125;
        double internalRad = 20;
        
        for (int i = 0; i < n; i++){
                double groupX = (externalRad*Math.sin( (steps[i]+90)*(Math.PI/180) )) + (canvasCenterX);
                double groupY = (externalRad*Math.cos( (steps[i]+90)*(Math.PI/180) )) + (canvasCenterY);
                
                double circularX = groupX - canvasCenterX;
                double circularY = groupY - canvasCenterY;
                
                //if the amount of nodes in the group is zero, break to move on.
                //create nothing here
                if (groups[i] == 0)
                    continue;
                
                double[] groupSteps = linearSeperation(groups[i]);
                double angle = (180/Math.PI)*slopeToAngle(-circularX/circularY);
                System.out.println(angle+"-----");
                
                
                Node[] partiteGroup = new Node[groupSteps.length];
                int index = 0;
                
                
                
                for (double group: groupSteps){
                    double x = group + groupX;
                    double y = groupY;
                    
                    Node node = new Node(x,y,15,++nodeCount);
                    partiteGroup[index] = node;
                    nodes.add(node);
                    node.setColor(i);
                    nodePane.getChildren().add(node);
                    index++;
                }
                
                
                int mid = partiteGroup.length / 2;
                System.out.println(partiteGroup[mid].getLabel().getText());
                
                if (partiteGroup.length % 2 !=0){
                    for (int z = 0; z < mid; z++){
                        rotateNode(partiteGroup[mid], angle, partiteGroup[z]);
                    }
                    for (int z = mid+1; z < partiteGroup.length; z++){
                        rotateNode(partiteGroup[mid], angle, partiteGroup[z]);
                    }
                }
                else {
                    for (int z = 0; z < mid; z++){
                        rotateNode(groupX, groupY, angle, partiteGroup[z]);
                    }
                    for (int z = mid; z < partiteGroup.length; z++){
                        rotateNode(groupX, groupY, angle, partiteGroup[z]);
                    }
                }
                
                
                
                /*for (double group: groupSteps){
                    //double x = ((internalRad+(3*n))*Math.sin( (group)*(Math.PI/180) )) + (groupX);
                    //double y = ((internalRad+(3*n))*Math.cos( (group)*(Math.PI/180) )) + (groupY);
                    
                    Node node = new Node(x,y,15,++nodeCount);
                    nodes.add(node);
                    node.setColor(i);
                    nodePane.getChildren().add(node);
                }*/
                
                
            }
        
        //Connect all differently colored nodes
        for (int i = 0; i < nodes.size(); i++){
                for (int j = i+1; j < nodes.size(); j++){
                    if (nodes.get(i).getColor() != nodes.get(j).getColor()){
                    Connection con = connect(nodes.get(i),nodes.get(j));
                    cons.add(con);
                    connectionPane.getChildren().add(con);
                    }
                }
            }
    }
    
    //////////////ROTATION TOOLS
    ///////
    //
    
        public void rotateNode(Node baseNode, double angle, Node movedNode){
            double cx = baseNode.getCenterX();
            double cy = baseNode.getCenterY();
            
            double px = movedNode.getCenterX();
            double py = movedNode.getCenterY();
            
            double[] result = rotatePoint(cx,cy,angle,px,py);
            
            movedNode.moveNode(result[0], result[1]);
        }
        
        public void rotateNode(double cx, double cy, double angle, Node movedNode){
            
            double px = movedNode.getCenterX();
            double py = movedNode.getCenterY();
            
            double[] result = rotatePoint(cx,cy,angle,px,py);
            
            movedNode.moveNode(result[0], result[1]);
        }
    
        public double[] rotatePoint(double cx,double cy,double angle,double px , double py){
            double s = Math.sin(angle*(Math.PI/180));
            double c = Math.cos(angle*(Math.PI/180));

            // translate point back to origin:
            px -= cx;
            py -= cy;

            // rotate point
            double xnew = px * c - py * s;
            double ynew = px * s + py * c;

            // translate point back:
            px = xnew + cx;
            py = ynew + cy;
            double[] ans = {px,py};
            return ans;
            
        }

    ///
    //////////
    /////////////////
    
    
    //\\\\\\
    //\\\\\
    //\\\\
    //\\\
    //\\
    //\
    // IMPLEMENTATION OF GRAPH UTILITY FUNCTIONS
    
    /////////
    // TRAVERSALS
    
    public ArrayList<Node> breadthFirstRecur(Node root,ArrayList<Node> traversed){
       if (!traversed.contains(root))
           traversed.add(root);
       for (int i = 0; i < root.getNodes().size();i++){
           Node node = root.getNodes().get(i);
           if (!traversed.contains(node)){
               breadthFirstRecur(node,traversed);
           }
       }
       return traversed;
    }
    
    public ArrayList<Node> breadthFirst(Node root){
        ArrayList<Node> traversed = new ArrayList<>();
        return breadthFirstRecur(root,traversed);
    }
    
    
    
    
    //CHECK WHETHER GRAPH IS CONNECTED
    public boolean graph_connected(){
        return (breadthFirst(nodes.get(0)).size() == nodes.size());
    }
    
    
    
    
    
    
    
    
    
    public static void main(String[] args) {
        launch(args);
    }
    
}
